# Intrepid Discovery

Algorithmic Discovery for Trading

## Overview: Discovery

The discovery feature set of Intrepid will use the robin-hood API to select companies whose options are poised for success.

## Overview: Process

1. Collect all available options from the API
2. Select only the options that show 5 year net-positive growth
3. From previous selection, select options that show a net-positive 1 year growth
4.
