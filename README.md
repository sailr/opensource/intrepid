# intrepid

Algorithmic Trading

## Overview

Intrepid uses robin-stocks to access Robinhood's API to make trades on my behalf. The central thesis behind the project is that the trades we make, while uneducated, follow a simple set of preconceived intuitions based on trajectory, recent news, and understanding of the industry. It is our theory that, because we could describe to you the rules for which we make trades, we therefore can automate the process.

## Getting Started

## Supporting

* robin_stocks [github](https://github.com/jmfernandes/robin_stocks)
* robin_stocks [documentation](http://www.robin-stocks.com/en/latest/)
