from discovery import query, analysis


def main():
  """entry function"""
  query.search()
  analysis.analyze()
  print("hello world")


if __name__ == "__main__":
  main()
